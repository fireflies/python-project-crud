from django.shortcuts import redirect

def landing_redirect(request):
    return redirect('/account')