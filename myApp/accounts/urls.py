from django.urls import path
from . import views
from django.contrib.auth.views import login, logout, password_reset, password_reset_done, password_reset_confirm, password_reset_complete
urlpatterns = [
    path('', views.landing),
    path('home/', views.home),
    path('login/', login, {'template_name': 'accounts/login.html'}),
    path('logout/', logout, {'template_name': 'accounts/logout.html'}),
    path('register/', views.register),
    path('profile/', views.profile),
    path('profile/edit/', views.editProfile),
    path('change-password/', views.change_password),
    path('reset-password/', password_reset),
    path('reset-password/done/',password_reset_done, name='password_reset_done'),
    path('reset-password/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/', password_reset_confirm, name='password_reset_confirm'),
    path('reset-password/complete/', password_reset_complete, name='password_reset_complete')
]