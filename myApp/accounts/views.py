from django.shortcuts import render, redirect
from django.contrib.auth.forms import PasswordChangeForm
from .forms import RegistrationForm, EditProfileForm
from django.contrib.auth.models import User
# Create your views here.
def landing(request):

    return render(request, 'accounts/landing.html')

def home(request):

    return render(request, 'accounts/home.html')

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/account/login/')
    else:
        form = RegistrationForm()

        args = {'form': form}
        return render(request,'accounts/register.html', args)

def profile(request):
    args = {'user': request.user}
    return render(request, 'accounts/profile.html', args)

def editProfile(request):
    if request.method == "POST":
        form = EditProfileForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect('/account/profile')

    else:
        form = EditProfileForm(instance=request.user)
        args = {'form': form}
        return render(request,'accounts/edit_profile.html', args)

def change_password(request):
    if(request.method == "POST"):
        form = PasswordChangeForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            return redirect('/account/profile')
        else:
            return redirect('/account/change-password')
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form}
        return render(request, 'accounts/change_password.html', args)
